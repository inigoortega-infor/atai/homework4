#!/usr/bin/env sh

par1="lib/jade.jar:lib/jadeTools.jar:lib/Base64.jar:lib/http.jar:lib/iiop.jar:lib/beangenerator.jar:lib/jgomas.jar:lib/jason.jar:lib/JasonJGomas.jar:classes:."

par2="jade.Boot -container -local-host 127.0.0.1"

ag_ax1="T1:es.upv.dsic.gti_ia.JasonJGomas.BasicTroopJasonArch(jasonAgent_AXIS.asl)"
ag_ax2="T2:es.upv.dsic.gti_ia.JasonJGomas.BasicTroopJasonArch(jasonAgent_AXIS.asl)"
ag_ax3="T3:es.upv.dsic.gti_ia.JasonJGomas.BasicTroopJasonArch(jasonAgent_AXIS.asl)"
ag_ax4="T4:es.upv.dsic.gti_ia.JasonJGomas.BasicTroopJasonArch(jasonAgent_AXIS.asl)"
ag_ax5="T5:es.upv.dsic.gti_ia.JasonJGomas.BasicTroopJasonArch(jasonAgent_AXIS.asl)"
ag_ax6="T6:es.upv.dsic.gti_ia.JasonJGomas.BasicTroopJasonArch(jasonAgent_AXIS_MEDIC.asl)"
ag_ax7="T7:es.upv.dsic.gti_ia.JasonJGomas.BasicTroopJasonArch(jasonAgent_AXIS_MEDIC.asl)"
ag_al1="A1:es.upv.dsic.gti_ia.JasonJGomas.BasicTroopJasonArch(jasonAgent_ALLIED_ROCKYBALBOA.asl)"
ag_al2="A2:es.upv.dsic.gti_ia.JasonJGomas.BasicTroopJasonArch(jasonAgent_ALLIED_ROCKYBALBOA.asl)"
ag_al3="A3:es.upv.dsic.gti_ia.JasonJGomas.BasicTroopJasonArch(jasonAgent_ALLIED_ROCKYBALBOA.asl)"
ag_al4="A4:es.upv.dsic.gti_ia.JasonJGomas.BasicTroopJasonArch(jasonAgent_ALLIED_ROCKYBALBOA.asl)"
ag_al5="A5:es.upv.dsic.gti_ia.JasonJGomas.BasicTroopJasonArch(jasonAgent_ALLIED_ROCKYBALBOA.asl)"
ag_al6="A6:es.upv.dsic.gti_ia.JasonJGomas.BasicTroopJasonArch(jasonAgent_ALLIED_ROCKYBALBOA.asl)"
ag_al7="A7:es.upv.dsic.gti_ia.JasonJGomas.BasicTroopJasonArch(jasonAgent_ALLIED_RAMBO.asl)"

agentes="$ag_ax1;$ag_ax2;$ag_ax3;$ag_ax4;$ag_ax5;$ag_ax6;$ag_ax7;$ag_al1;$ag_al2;$ag_al3;$ag_al4;$ag_al5;$ag_al6;$ag_al7"


java -classpath "$par1" $par2 "$agentes"
